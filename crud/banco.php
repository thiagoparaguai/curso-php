<?php
class Banco {
    
    private $pdo;

    public function __construct($host, $dbname, $dbuser, $dbpass) {
        try {
            $this->pdo = new PDO("mysql:dbname=".$dbname.";host=".$host, $dbuser, $dbpass);
        } catch(PDOException $e){
            echo "Falhou : ".$e->getMessage();
        }
    }

    /* Método query responsavel por
    manipular o banco de dados */
    public function query($sql) {
        $query = $this->pdo->query($sql);
        $this->numRows = $query->rowCount();
        $this->array = $query->fetchAll();
    }

    public function result() {
        return $this->array;
    }

    public function numRows() {
        return $this->numRows;
    }

    public function insert($table, $data) {
        if(!empty($table) && ( is_array($data) && count($data) > 0 )) {
            $sql = "INSERT INTO ".$table." SET ";

            // INSERT INTO tabela SET nome = 'fulano', idade = 90;
            $dados = array();
            foreach($data as $chaves => $valor) {
                $dados[] = $chaves." = '".addslashes($valor)."'";
            }

            $sql = $sql.implode(", ", $dados);
            $this->pdo->query($sql);                    
        }
    }

    
}
?>