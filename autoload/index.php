<?php
// Criando autoload

// Forma mais usada até pouco tempo atrás
// function __autoload($class) {
	// include_once($class.'.class.php');
// }

// Nova forma de definir o autoload
spl_autoload_register(function ($class) {
	
	if (file_exists('classes/'.$class.'.class.php')):
		require_once('classes/'.$class.'.class.php');
	endif;
});

$obj = new bola();
$obj->setCor('azul');

echo "A cor da bola é : ".$obj->getCor()."<br/><br/>";

$campo = new campo();
$campo->setDados(20, 30, 40);

echo "Me dê a largura: ".$campo->getLargura();
