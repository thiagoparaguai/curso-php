<?php
/**
 * Page-Level DocBlock example.
 * displays as Gregory Beaver<strong>cellog@php.net</strong>
 * , where underlined text is a "mailto:cellog@php.net" link
 * @author Thiago Lázaro <thiagolazaro2009@gmail.com>
 */
require 'config.php';

class Usuario {

    /* Propriedades da classe */
    
    private $id;
    private $nome;
    private $email;
    private $senha;

    private $pdo;

    /* Criando construtor da classe usuario */

    public function __construct($i=0) {
        try {
			$this->pdo = new PDO('mysql:host=' . MYSQL_HOST . ';dbname=' . MYSQL_DBNAME, MYSQL_USER, MYSQL_PASSWORD);
        } catch(PDOException $e) {
            echo "Falhou: " .$e->getMessage();
        }

        if(!empty($i)) {            
            // Excutando a função para pegar as informações desse usuario em particular
            $sql = "SELECT * FROM usuarios WHERE id = ?";
            $sql = $this->pdo->prepare($sql);
            $sql->execute(array($i));

            // Verifica se existe algum valor ao executar
            if ($sql->rowCount() > 0) {
               $data = $sql->fetch();
               $this->id = $data['id'];
               $this->email = $data['email'];
               $this->senha = $data['senha'];
               $this->nome = $data['nome'];
            }
        }
    }
	/* End Construct */

    /* Métodos da classe Usuarios
    propriedades usando get e set */

    public function getId() {
        return $this->id;
    }

    public function setNome($n) {
        $this->nome = $n;
    }
    public function getNome() {
        return $this->nome;
    }

    public function setEmail($e) {
        $this->email = $e;
    }
    public function getEmail() {
        return $this->email;
    }

    public function setSenha($s) {
        $this->senha = md5($s);
    }
    public function getSenha() {
        return $this->senha;
    }


    // Método Salvar
    public function salvar() {
		
        // Verifica se existe usuario pelo se existe realiza update senão cria usuario
        if (!empty($this->id)):
            $sql = "UPDATE usuarios SET 
                email = ?, 
                senha = ?, 
                nome = ? 
                WHERE id = ?";

            $sql = $this->pdo->prepare($sql);
            $sql->execute(array(
                $this->email,
                $this->senha,
                $this->nome, 
                $this->id));

        else :
            $sql = "INSERT INTO usuarios SET
				email = ?, 
				senha = ?, 
				nome = ?";
            $sql = $this->pdo->prepare($sql);
            $sql->execute(array(
                $this->email,
                $this->senha,
                $this->nome));

        endif;
		
    }
	/* End Salvar */
	
    
    /**
     * Método Delete
     *
     * Este Método {Delete (int)} deleta o usuario pelo id
     */
	public function Delete () {
		$sql = "DELETE FROM usuarios WHERE id = ?";
		$sql = $this->pdo->prepare($sql);
		$sql->execute(array($this->id));
	}
	
}
/* End Class Usuario */

	
	
?>

