<?php
// Controller base 
class controller {
	
	// Método de chamar View - passando os parametros nome, data através do array
	public function loadView($viewName, $viewData = array()) {
		
/* 		$viewData = array(
			'nome' => 'Thiago',
			'idade' => 28
		); */
		
/* 		o Extract transfoma o array em variaveis exemplo array acima
		echo nome; */
		
		extract($viewData);
		include 'views/'.$viewName.'.php';
		
	}
	
	/* Criando intermediador entre o loadView
	ou seja "Não vou carregar diretamente a 
	pagina que quero acesar vou carregar o templete
	e o template pega minha pagina dentro destra estrutura
	para carregar o template" */
	
	/* A estrutura da mesma loadView a unica diferença é que aqui dentro
    única diferença é que carregaremos o template */
	
	public function loadTemplate($viewName, $viewData = array()) {
		include 'views/template.php';
	}
	
	// Método para carregar o view  dentro template
	public function loadViewInTemplate($viewName, $viewData = array()) {
		extract($viewData);
		include 'views/'.$viewName.'.php';
		
	}
}
