<?php
class homeController extends controller {
	
	public function index() {
		$usuario = new Usuario();
		$usuario->setName('Thiago');
		
		$dados = array(
			'name' => $usuario->getName()
		);
		
		// Chamando a loadView
		$this->loadTemplate('home', $dados);
	}
	
	public function sobre() {
		$dados = array();
		$this->loadTemplate('sobre', $dados);
	}
}